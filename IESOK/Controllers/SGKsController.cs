﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IESOK.Models;

namespace IESOK.Controllers
{
    public class SGKsController : Controller
    {
        private KUBAEntities db = new KUBAEntities();

        // GET: SGKs
        public ActionResult Index()
        {
            return View(db.SGK.ToList());
        }

        // GET: SGKs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SGK sGK = db.SGK.Find(id);
            if (sGK == null)
            {
                return HttpNotFound();
            }
            return View(sGK);
        }

        // GET: SGKs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SGKs/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDGGK,NAZWAG")] SGK sGK)
        {
            if (ModelState.IsValid)
            {
                db.SGK.Add(sGK);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sGK);
        }

        // GET: SGKs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SGK sGK = db.SGK.Find(id);
            if (sGK == null)
            {
                return HttpNotFound();
            }
            return View(sGK);
        }

        // POST: SGKs/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDGGK,NAZWAG")] SGK sGK)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sGK).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sGK);
        }

        // GET: SGKs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SGK sGK = db.SGK.Find(id);
            if (sGK == null)
            {
                return HttpNotFound();
            }
            return View(sGK);
        }

        // POST: SGKs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SGK sGK = db.SGK.Find(id);
            db.SGK.Remove(sGK);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
